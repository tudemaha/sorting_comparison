import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Heap {
    public int[] open_file(String file_name) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file_name));
        String line = br.readLine();
        String raw[] = line.split(",");
        br.close();

        int[] data = new int[raw.length];
        for(int i = 0; i < raw.length; i++) {
            data[i] = Integer.parseInt(raw[i]);
        }
        
        return data;
    }

    public void max_heapify(int[] data, int count, int i) {
        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if(left < count && data[largest] < data[left]) {
            largest = left;
        }

        if(right < count && data[largest] < data[right]) {
            largest = right;
        }

        if(largest != i) {
            int temp = data[i];
            data[i] = data[largest];
            data[largest] = temp;
            max_heapify(data, count, largest);
        }
    }

    public void heap(int[] data) {
        int n = data.length;

        for(int i = n / 2 - 1; i >= 0; i--) {
            max_heapify(data, n, i);
        }

        for(int i = n - 1; i > 0; i--) {
            int temp = data[0];
            data[0] = data[i];
            data[i] = temp;
            max_heapify(data, i, 0);
        }
    }

    public static void main(String[] args) throws IOException {
        Heap ob = new Heap();

        int[] data = ob.open_file("random.csv");

        int num_data = 1000;
        int[] new_data = new int[num_data];
        for(int i = 0; i < num_data; i++) {
            new_data[i] = data[i];
        }

        double start = System.nanoTime();
        ob.heap(new_data);
        double end = System.nanoTime();

        double time = end - start;

        System.out.println("\nNumber of data: " + num_data);
        System.out.println("Running time: " + (double)time / 1000000000 + " seconds\n");
        System.out.println(Arrays.toString(new_data));
    }
}