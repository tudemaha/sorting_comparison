import time
from copy import deepcopy

def open_file(file_name):
    raw = open(file_name, "r")
    raw = raw.readlines()
    raw = raw[0].split(',')

    data = []
    for i in range(len(raw)):
        data.append(int(raw[i]))
    
    return data

def max_heapify(data, count, i):
    largest = i
    left = 2 * i + 1
    right = 2 * i + 2

    if left < count and data[largest] < data[left]:
        largest = left
    
    if right < count and data[largest] < data[right]:
        largest = right

    if largest != i:
        data[i], data[largest] = data[largest], data[i]
        max_heapify(data, count, largest)

def heap_sort(data):
    count = len(data)

    for i in range( count // 2 - 1, -1, -1):
        max_heapify(data, count, i)

    for i in range(count - 1, 0, -1):
        data[i], data[0] = data[0], data[i]
        max_heapify(data, i, 0)
    

def main():
    data = open_file("random.csv")

    
    num_data = 1000
    data = deepcopy(data[:num_data])

    start = time.time()
    heap_sort(data)
    end = time.time()

    duration = end - start

    print(f"\nNumber of data: {num_data}")
    print(f"Running time: {duration} seconds\n")
    print("Sorted data:", data)

if __name__ == "__main__":
    main()