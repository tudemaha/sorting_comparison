import time

def open_file(file_name):
    raw = open(file_name, "r")
    raw = raw.readlines()
    raw = raw[0].split(',')

    data = []
    for i in range(len(raw)):
        data.append(int(raw[i]))
    
    return data

def insertion(data):
    n = len(data)

    for i in range(1, n):
        key = data[i]
        j = i - 1
        while j >= 0 and data[j] > key:
            data[j + 1] = data[j]
            j -= 1
        data[j + 1] = key

def main():
    data = open_file("random.csv")

    start = time.time()
    insertion(data)
    end = time.time()

    duration = end - start

    print(f"Running time: {duration} seconds\n")
    print(data)

if __name__ == "__main__":
    main()