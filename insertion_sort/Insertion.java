import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Insertion {
    public int[] open_file(String file_name) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file_name));
        String line = br.readLine();
        String raw[] = line.split(",");
        br.close();

        int[] data = new int[raw.length];
        for(int i = 0; i < raw.length; i++) {
            data[i] = Integer.parseInt(raw[i]);
        }
        
        return data;
    }

    public void insertion(int[] data) {
        int n = data.length;

        for (int i = 1; i < n; i++) {
            int key = data[i];
            int j = i - 1;
            while (j >= 0 && data[j] > key) {
                data[j + 1] = data[j];
                j--;
            }
            data[j + 1] = key;
        }
    }

    public static void main(String[] args) throws IOException {
        Insertion ob = new Insertion();

        int[] data = ob.open_file("random.csv");

        double start = System.nanoTime();
        ob.insertion(data);
        double end = System.nanoTime();

        double time = end - start;
        System.out.println("Running time: " + (double)time / 1000000000 + " seconds\n");
        System.out.println(Arrays.toString(data));
    }
}